const chai = require('chai');
const { expect } = chai;

const HierarchyInspector = require('../src/hierarchy-inspector');

const GraphWeaver = require('../src/graph-weaver');

const subject = new HierarchyInspector();

const data = {
    'horizontal.head.transform': 'upperCase',
    'vertical.label.width': 8,
    'vertical.cell.width': 10,
    'vertical.cell.align': 'right',
    'id.horizontal.width': 6,
    'name.horizontal.width': 12,
    'opening.cell.transform': 'twoDecimals',
    'opening.horizontal.width': 10,
    'opening.horizontal.cell.align': 'right',
    'type.horizontal.width': 8,
    'token.horizontal.width': 12,
    'drawee.horizontal.width': 12,
    'assignee.horizontal.width': 12,
    'account.horizontal.width': 12,
    'tags.horizontal.width': 20,
    'tags.cell.transform': 'flattenTags',
    'operationDate.horizontal.width': 10,
    'operationDate.cell.transform': 'isoDate',
    'operationDate.transform': 'OP. DATE',
    'amount.horizontal.width': 12,
    'amount.horizontal.cell.align': 'right',
    'amount.cell.transform': 'twoDecimals',
    'currency.horizontal.width': 3,
    'times.transform': 'PERIOD',
    'times.cell.transform': 'explainTime',
    'times.horizontal.width': 6,
    'periodLength.transform': ' ',
    'periodLength.cell.transform': 'explainPeriodLength',
    'periodLength.horizontal.width': 6,
    'period.transform': ' ',
    'period.cell.transform': 'explainPeriod',
    'period.horizontal.width': 6
};

const graph = new GraphWeaver().atRoot()
    .addBranch('id', 'horizontal', 'cell', 'width')
    .addBranch('amount', 'horizontal', 'cell', 'align')
    .addBranch('opening', 'horizontal', 'cell', 'transform')
    .addBranch('opening', 'vertical', 'cell', 'transform')
    .addBranch('opening', 'horizontal', 'cell', 'align')
    .addBranch('opening', 'vertical', 'cell', 'align')
    .weave();

describe('HierarchyInspector', () => {
    describe('#inspect(fieldSpecs)', () => {
        it('Should return from an exact match', () => {
            const cfg = subject.inspect(data);
            expect(cfg.id.horizontal.cell.width.value).to.be.equal(6);
        });
        it('Should return from general default value', () => {
            const cfg = subject.inspect(data);
            expect(cfg.amount.horizontal.cell.align.value).to.be.equal('right');
        });

        it('Should return opening.horizontal.cell.transform', () => {
            const cfg = subject.inspect(data);
            expect(cfg.opening.horizontal.cell.transform.value).to.be.equal('twoDecimals');
        });

        it('Should return opening.vertical.cell.transform', () => {
            const cfg = subject.inspect(data);
            expect(cfg.opening.vertical.cell.transform.value).to.be.equal('twoDecimals');
        });

        it('Should return opening.horizontal.cell.align', () => {
            const cfg = subject.inspect(data);
            expect(cfg.opening.horizontal.cell.align.value).to.be.equal('right');
        });

        it('Should return opening.vertical.cell.align', () => {
            const cfg = subject.inspect(data);
            expect(cfg.opening.vertical.cell.align.value).to.be.equal('right');
        });
    });

    describe('#inspectBoundByGraph(fieldSpecs,graph)', () => {
        it('Should return from an exact match', () => {
            const cfg = subject.inspectBoundByGraph(data, graph);
            expect(cfg.id.horizontal.cell.width).to.be.equal(6);
        });
        it('Should return from general default value', () => {
            const cfg = subject.inspectBoundByGraph(data, graph);
            expect(cfg.amount.horizontal.cell.align).to.be.equal('right');
        });

        it('Should return opening.horizontal.cell.transform', () => {
            const cfg = subject.inspectBoundByGraph(data, graph);
            expect(cfg.opening.horizontal.cell.transform).to.be.equal('twoDecimals');
        });

        it('Should return opening.vertical.cell.transform', () => {
            const cfg = subject.inspectBoundByGraph(data, graph);
            expect(cfg.opening.vertical.cell.transform).to.be.equal('twoDecimals');
        });

        it('Should return opening.horizontal.cell.align', () => {
            const cfg = subject.inspectBoundByGraph(data, graph);
            expect(cfg.opening.horizontal.cell.align).to.be.equal('right');
        });

        it('Should return opening.vertical.cell.align', () => {
            const cfg = subject.inspectBoundByGraph(data, graph);
            expect(cfg.opening.vertical.cell.align).to.be.equal('right');
        });
    });
});
