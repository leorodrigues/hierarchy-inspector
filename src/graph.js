module.exports = class Graph {
    constructor(root, branches) {
        Object.assign(this, { _root: root, branches });
    }

    get root() {
        return this._root.sprout();
    }

    contains(branch) {
        return this.branches.some(b => b.equals(branch));
    }
};
