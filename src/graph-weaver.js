const Branch = require('./branch');
const Graph = require('./graph');

module.exports = class GraphWeaver {
    constructor() {
        this.branches = [ ];
    }

    atRoot(...path) {
        this.root = new Branch(...path);
        return this;
    }

    addBranch(...path) {
        this.branches.push(this.root.sprout(...path));
        return this;
    }

    weave() {
        return new Graph(this.root, this.branches);
    }
};