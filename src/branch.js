module.exports = class Branch {
    constructor(...path) {
        this.path = path;
    }

    equals(anotherBranch) {
        return Branch.samePath(this.path, anotherBranch.path);
    }

    sprout(...path) {
        return new Branch(...this.path, ...path);
    }

    [Symbol.iterator]() {
        return this.path[Symbol.iterator]();
    }

    static samePath(onePath, anotherPath) {
        return Branch.sameObject(onePath, anotherPath)
            || Branch.sameLength(onePath, anotherPath)
            && Branch.sameContent(onePath, anotherPath);
    }

    static sameObject(onePath, anotherPath) {
        return onePath === anotherPath;
    }

    static sameLength(onePath, anotherPath) {
        return onePath.length === anotherPath.length;
    }

    static sameContent(onePath, anotherPath) {
        return onePath.every((v, i) => v === anotherPath[i]);
    }
};
