module.exports = class HierarchyInspector {
    constructor() {
        const dataPaths = [ ];
        Object.assign(this, { dataPaths });
    }

    inspect(object) {
        return new Proxy({ object }, HANDLERS);
    }

    inspectBoundByGraph(object, graph) {
        return new Proxy({ object, graph }, BOUND_HANDLERS);
    }
};

const HANDLERS = {
    get: function(target, property) {
        const { object, path = [ ] } = target;

        if (property === 'value')
            return resolveValuePath(object, path);

        target = { object, path: [...path, property] };
        return new Proxy(target, HANDLERS);
    }
};

const BOUND_HANDLERS = {
    get: function(target, property) {
        const { object, graph } = target;
        let { branch = graph.root } = target;

        branch = branch.sprout(property);

        if (graph.contains(branch))
            return resolveValuePath(object, branch);

        target = { object, graph, branch };
        return new Proxy(target, BOUND_HANDLERS);
    }
};

function resolveValuePath(object, pathQuery) {
    let lastSimilarity = 0;
    let lastPath = '';
    for (const path in object) {
        const similarity = computeSimilarity(path, pathQuery);
        if (similarity > lastSimilarity) {
            lastSimilarity = similarity;
            lastPath = path;
        }
    }
    return object[lastPath];
}

function computeSimilarity(path, pathQuery) {
    const weight = computeWeight(path, pathQuery);
    path = path.split('.');
    path.unshift('root');
    pathQuery = ['root', ...pathQuery];
    let pathEnd = path.length - 1;
    let queryEnd = pathQuery.length - 1;
    while (path[pathEnd] === pathQuery[queryEnd]) {
        if (pathEnd === 0 || queryEnd === 0) break;
        pathEnd--;
        queryEnd--;
    }

    if (pathEnd + queryEnd === 0)
        return 1;

    if ((pathEnd - path.length + 1) + (queryEnd - pathQuery.length + 1) === 0)
        return 0;

    const start = findForwardDivergence(path, pathQuery);

    const distance = computeDistance(start, queryEnd + 1, path, pathQuery);

    return (1 / (distance * (1 + weight)));
}

function findForwardDivergence(path, pathQuery) {
    let start = 0;
    while (path[start] === pathQuery[start]) {
        if (start === path.length || start === pathQuery.length) break;
        start++;
    }
    return start;
}

function computeWeight(path, pathQuery) {
    return path.startsWith(pathQuery[0]) ? 0 : 1;
}

function computeDistance(start, queryEnd, path, pathQuery) {
    const distance = pathQuery.length - queryEnd;
    for (let offset = 0; offset < distance; offset++)
        if (path[start + offset] !== pathQuery[queryEnd + offset])
            return Infinity;
    return Math.abs(pathQuery.length - path.length) + 1;
}
