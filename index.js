module.exports = {
    Graph: require('./src/graph'),
    Branch: require('./src/branch'),
    GraphWeaver: require('./src/graph-weaver'),
    HierarchyInspector: require('./src/hierarchy-inspector')
};